const mongoose = require("mongoose");
const { Schema } = mongoose;

const ItemSchema = new Schema({
  title: { type: String, required: true },
  username: { type: String, required: true },
  password: { type: String, required: true },
  url: { type: String, required: true },
  commentary: { type: String, required: false },
  date: { type: Date, default: Date.now },
});

module.exports = mongoose.model("Item", ItemSchema);
