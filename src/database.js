const mongoose = require("mongoose");

// connection to db
mongoose
  .connect("mongodb://localhost/DB", {
    useCreateIndex: true,
    useNewUrlParser: true,
    useFindAndModify: false,
  })
  .then((db) => console.log("DB Connected"))
  .catch((err) => console.log(err));
