// rutas
const express = require("express");
const router = express.Router();

router.get("/users/intro", (req, res) => {
  res.render("users/user_page");
});

module.exports = router;
