// rutas
const express = require("express");
const router = express.Router();

const Item = require("../models/Item");

router.get("/items/add", (req, res) => {
  res.render("items/new_item");
});

router.post("/items/new-item", async (req, res) => {
  const { title, username, password, url, commentary } = req.body;
  // console.log(req.body);
  const newItem = new Item({ title, username, password, url, commentary });
  await newItem.save();
  res.redirect("/items");
});

router.get("/items", async (req, res) => {
  const items = await Item.find();
  res.render("items/all_items", { items });
});

module.exports = router;
